# arrays
Solutions to algorithm problems that use the basic principles of arrays and slices.

## Two Number Sum

Write a function that takes a non-empty array of distinct integers and an integer representing a target sum. If any two numbers in the input array sum up to the target number, the function should return them in a sorted array. If no two numbers sum up to the target number, the function should return an empty array.

__Sample input:__ `[3,5,-4,8,11,1,-1,6], 10`
__Sample output:__ `[-1,11]`

## subarray sort

Write a function that takes in an array of integers of length at least 2. The function should return an array of the starting and ending indices of the smallest subarray in the input array that needs to be sorted in order for the entire array to be sorted. If the input array is already sorted, return `[-1,-1]`. 

__Sample input:__ `[1,2,4,7,10,11,7,12,6,7,16,18,19]`
__Sample output:__ `[3,9]`
