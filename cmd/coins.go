// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/apex/log"
	"github.com/spf13/cobra"
	"gitlab.com/mshindle/tidbits/dynamic"
)

// coins value to calculate
var value int

// coinsCmd represents the coins command
var coinsCmd = &cobra.Command{
	Use:   "coins",
	Short: "find min number of coins needed to make a value",
	Long: `Dynamic programming example to determine the minimum number 
of coins needed to fulfill a cents value.

The coin denominations are 1, 5, 10, 25, and 100.`,
	Run: func(cmd *cobra.Command, args []string) {
		d := []int{1, 5, 10, 25, 100}
		logger := log.WithField("value", value).WithField("d", d)
		logger.Info("running coins")
		nc := dynamic.Coins(value, d)
		logger.WithField("num_coins", nc).Info("number of coins")
	},
}

func init() {
	rootCmd.AddCommand(coinsCmd)
	coinsCmd.Flags().IntVarP(&value, "value", "v", 26, "value in cents to have coins sum to")
}
