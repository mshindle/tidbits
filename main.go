package main

import (
	"gitlab.com/mshindle/tidbits/cmd"
)

func main() {
	cmd.Execute()
}
